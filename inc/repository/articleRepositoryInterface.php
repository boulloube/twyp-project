<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 22/03/2016
 * Time: 13:58
 */

namespace repository;


use model\Article;

interface ArticleRepositoryInterface
{
    public function selectAll();
    public function select($id);
    public function create(Article $article);
    public function update(Article $article);
    public function delete(Article $article);
}
<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 22/03/2016
 * Time: 14:01
 */

namespace repository;

use model\Article;
use Slim\PDO\Database;

class MySQLArticleRepository implements ArticleRepositoryInterface
{
    protected $pdo;

    const DBNAME = 'twyp_database';
    const HOST = 'localhost';
    const USER = 'root';
    const PASSWD = '';
    const TABLENAME = 'article';

    public function __construct()
    {
        $this->pdo = new Database('mysql:dbname=' . self::DBNAME . ';host=' . self::HOST, self::USER, self::PASSWD);
    }

    public function selectAll()
    {
        $query = $this->pdo->select()
            ->from(self::TABLENAME);

        $stmt = $query->execute();
        
        return $stmt->fetch();
    }

    public function select($id)
    {
        $query = $this->pdo->select()
            ->from(self::TABLENAME)
            ->where('id', '=', $id);

        $stmt = $query->execute();
        
        return $stmt->fetchObject(Article::class);
    }

    public function create(Article $article)
    {
        $lastId = $this->pdo->lastInsertId();

        $query = $this->pdo->insert(array('id', 'title', 'url', 'content', 'image', 'user'))
            ->into(self::TABLENAME)
            ->values(array(++$lastId, $article->title, $article->url, $article->content, $article->img, 1)); //TODO : get user id

        return $query->execute(false);
    }

    public function update(Article $article)
    {
        $query = $this->pdo->update(array(
            'title'     => $article->title,
            'url'       => $article->url,
            'content'   => $article->content,
            'image'     => $article->img,
            'read'      => $article->read,
            'liked'     => $article->liked,
            'archived'  => $article->archived
        ))
            ->table(self::TABLENAME)
            ->where('id', '=', $article->id);

        return $query->execute();
    }

    public function delete(Article $article)
    {
        $query = $this->pdo->delete()
            ->where('id', '=', $article->id);

        return $query->execute();
    }
}
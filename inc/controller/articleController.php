<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 22/03/2016
 * Time: 16:39
 */

namespace controller;


use model\Article;
use repository\MySQLArticleRepository;

class ArticleController
{
    private $repository;

    public function __construct()
    {
        $this->repository = new MySQLArticleRepository();
    }

    public function selectAll()
    {
        // TODO: Implement selectAll() method
    }

    public function select($id)
    {
        return $this->repository->select($id);
    }

    public function create(Article $article)
    {
        // TODO: Implement create() method.
    }

    public function update(Article $article)
    {
        // TODO: Implement update() method.
    }

    public function delete(Article $article)
    {
        // TODO: Implement delete() method.
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Sylvain
 * Date: 10/02/2016
 * Time: 20:58
 */

namespace model;


class Article
{
    public $id;
    public $title;
    public $url;
    public $content;
    public $read;
    public $archived;
    public $liked;
    public $image;
    public $category;
    public $user;
}


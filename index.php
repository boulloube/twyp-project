<?php


use model\Article;

require 'vendor/autoload.php';
require 'inc/app.php';
require_once 'inc/model/article.php';
//require 'inc/config.php';

$container = $api->app->getContainer();
$container['view'] = new \Slim\Views\PhpRenderer("./src/templates/");

$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};


$api->app->get('/test', function($request, $response, $args){
    $retVar = "Hola!";

    $response = $this->view->render($response, "test.phtml", ["text" => $retVar]);
    return $response;
});

$api->app->get('/', function($request, $response, $args){
    $retVar = new Article();
    $retVar->title = "test";
    //$this->logger->addInfo("HOME : request / " . $request . "/");
    // is connected
        // retrieve user's articles
    // else
        // log in page

    $response = $this->view->render($response, "homepage.phtml", ["articles" => $retVar]);
    return $response;
});

$api->app->get('/save', function($request, $response, $args){

});

$api->run();